<?php
namespace Project\Helpers;

use A365\Wordpress\Models\Page;
use Project\Helpers\MenuElementHelper;

class MenuHelper extends \A365\Wordpress\Helpers\MenuHelper
{
    private $_with_submenu_div = false;
    private $_depth = 3;
    private $_elements = array();
    private $_branch = array();

    public function getMenuItems( $menu_id, $config = array() )
    {
        
        if (isset($config["with_submenu_div"])) {
            $this->_with_submenu_div = $config["with_submenu_div"];
        }
        if (isset($config["depth"])) {
            $this->_depth = $config["depth"];
        }

        if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_id ] ) ) {
            $menu = wp_get_nav_menu_object( $locations[ $menu_id ] );
            $menu_id = $menu->term_id;
        }

        $this->_elements = wp_get_nav_menu_items( $menu_id );
        $this->_branch = $this->_elements ? $this->_buildTree() : null;
        $this->_branch = $this->_branch ? $this->_activateTree() : null;

        return $this->_branch;
    }

    private function _activateTree() {
        return $this->_branch;
    }


    private function _buildTree($parentMenuElement = null, $level = 0)
    {
        $branch = array();
        $level += 1;
        $branch_active = false;

        if (!$parentMenuElement) {
            $parentMenuElement = MenuElementHelper::create(0, $level);
        }
        
        foreach ( $this->_elements as &$element ) {


            if ( $element->menu_item_parent == $parentMenuElement->menu_item_id )
            {
                $config = array();
                $object_id = $element->object_id;
                if (function_exists("pll_get_post")) {
                    $object_id = pll_get_post($object_id);
                }
                $config["active"] = ($object_id == get_the_ID());
                if ($config["active"]) {
                    $branch_active = true;
                }
                $config["type"] = $element->type;
                $config["link"] = $element->url;
                $config["post_id"] = $element->object_id;
                
                $config["menu_item_parent"] = $element->menu_item_parent;
                $config["with_submenu_div"] = $this->_with_submenu_div;
                $config["title"] = $element->title;
                
                $menuElement = MenuElementHelper::create($element->ID, $level, $config);

                if ($level < $this->_depth) {
                    $branch_infos = $this->_buildTree($menuElement, $level );
                    $menuElement->addSubmenuPages($branch_infos['branch']);
                    //$menuElement->addHashSections(Page::find($element->object_id)->getHashSections(), $level, $element->object_id);
                    if ($branch_infos['branch_active']) {
                        $branch_active = true;
                        $menuElement->setActive(true);
                    }
                }

                $element = $menuElement;

                $branch[$element->menu_item_id] = $element;
                unset( $element );
            }
        }
        return ['branch' => $branch, 'branch_active' => $branch_active];
    }

}