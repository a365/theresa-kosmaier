<?php
$css = 'assets/css/main.min.css';

if (is_readable($css)) {
    $css .= '?' . filemtime($css);
}
?>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <base href="{{get_site_url()}}" />

    <title><?php
    if (is_search()) { echo __('Search for') . ' &quot;'.esc_html($_GET["s"]).'&quot; - '; }
    elseif (!(is_404()) && (is_single()) || (is_page())) { wp_title(''); }
    elseif (is_404()) { echo __('Not Found') . ' - '; bloginfo('name'); }
    else { wp_title(''); }
    ?></title>
    {{-- Styles --}}
    <link rel="stylesheet" href="{{$css}}" type="text/css" media="all">
    {{-- Typekit Fonts --}}
    <link rel="stylesheet" href="https://use.typekit.net/dpu1hxf.css">

    @if(!A365\Wordpress\Environment::isDevelop())
        @include('components.tracking')
    @endif

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link rel="alternate" hreflang="<?php language_attributes(); ?>" href="{{get_site_url()}}" />

    {{-- Scripts --}}
    <script type="text/javascript" src="assets/js/vendor/modernizr.min.js?{!!filemtime('assets/js/vendor/modernizr.min.js')!!}"></script>

    <?php wp_head(); ?>
    
    <?php
        $id = get_the_ID();
        $post_type = get_post_type();

        $og = new \Project\Helpers\OgHelper;
        
        echo $og->print_it();
        
    ?>

</head>