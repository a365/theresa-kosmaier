<?php
namespace Project\Sections;

use A365\Wordpress\Helpers\Acf\FieldHelper;

class Slider extends \A365\Wordpress\Block\AcfBlock {

	protected $_template = 'slider';
	protected static $_label = 'Gallerie Slider';

	/**
	 * @inheritdoc
	 */
	public static function getAcfSubfields()
	{
		return [
			FieldHelper::createText('title', 'Titel')->setRequired(),
			FieldHelper::createText('subtitle', 'Untertitel')->setRequired(),
			FieldHelper::createRepeater('gallery', 'Gallerie')
				->addSubfields([
					FieldHelper::createImage('image', 'Bild', ["library" => "all"])->setRequired(),
					FieldHelper::createWysiwyg('image-text', 'Bildbeschreibung')->setRequired(),
				]),
		];
	}
}