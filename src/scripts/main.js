(function(jQuery){

	/* ======== Extern ======== */
	//= include ../bower_components/owl.carousel/dist/owl.carousel.min.js
	//= include ../bower_components/velocity/velocity.min.js
	//= include ../bower_components/js-cookie/src/js.cookie.js
	//= include ../bower_components/bowser/src/bowser.js
	
	//= include extern/jquery.lazyloadxt.js

	/* ======== Common includes ======== */
	//= include common/AjaxForm.js
	//= include common/Application.js
	//= include common/Module.js
	
	/* ======== Module includes ======== */
	//= include modules/Alert.js
	//= include modules/AlertController.js
    //= include modules/OwlcarouselController.js
    //= include modules/Owlcarousel.js


	/* ======== Application Setup ======== */
	var application = new Application();
	application
		//.addModule(new AjaxFormController(), 'ajaxFormController')
		.addModule(new AlertController(), 'alertController')
        .addModule(new OwlcarouselController(), 'owlcarouselcontroller')
		.run();

})(jQuery);