<?php
	/**
	 * @var \Project\Sections\Test $block
	 */
	$title = $block->getTitle();
	$subtitle = $block->getSubtitle();
	$slider = $block->getGallery();

	use \Project\Helpers\ImageHelper;
	
?>
	
<section class="section section--margin">
	<div class="section__content">
		<div class="slider">
			<div class="slider__wrapper">
				<div class="slider__content js-owl-carousel">
					@foreach($slider as $slide)
						<?php 
							$image = ImageHelper::getImageUrlById($slide['image']);
						?>
						<div class="slider__content__item">
							<div class="slider__content__item__image">
								<img src="{!!$image!!}" alt="{{$title}}">
								<div class="slider__content__item__image__description">
									<div class="table table--fullheight">
										<div class="table__td editor-content">
											{!!$slide['image-text']!!}
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach

				</div>
				<h2 class="slider__title h1">{{$title}}</h2>
			</div>
			<div class="slider__subtitle">
				<div class="description">{{$subtitle}}</div>
			</div>	
		</div>
	</div>
</section>