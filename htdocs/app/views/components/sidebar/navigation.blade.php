<?php
$menuHelper = \Project\Helpers\MenuHelper::getInstance();
$nav_items      = $menuHelper->getMenuItems('primary');
?>
{{-- Navigation --}}
<nav class="nav nav--sidebar js-nested-navigation">
    <ul class="nav__list nav__list--level-1">
        @each("components.navigation.item", $nav_items['branch'], 'nav_item')
    </ul>
</nav>