<?php
namespace Project\Helpers;

class RequestHelper extends \A365\Core\Abstracts\Helper
{

    public static function _POST($name){
        return self::param($name);
    }


    public static function _GET($name){
        return self::param($name, "get");
    }

    public static function param($name, $type = "post") {
        $param = &$GLOBALS["_" . strtoupper($type)][$name];
        return self::filter_string($param);
    }
        
    public static function getAllParams(){
        $return = array();
        foreach($_POST as $k => $v){
            $return[$k] = self::filter_string($v);
        }
        
        return (object) $return;
    }

    public static function filter_string($string) {
        if (is_array($string)) {
            array_walk_recursive($string, "self::filter_string");
            return $string;
        }
        return !empty($string) ? htmlspecialchars($string, ENT_QUOTES, "UTF-8") : "";
    }

}