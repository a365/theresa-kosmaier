<?php
use A365\Wordpress\Helpers\Acf\OptionsHelper;

$label_message = __('Your Message');
$label_thank = __('Thank you for your message');
$label_soon = __('We will respond as soon as possible.');
$label_greets = __('Best regards,');
$label_company = __('');

$logo = Project\Application::getInstance()->getConfig('media.logo-png');
$client = Project\Application::getInstance()->getConfig('client');

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<h2>{{$label_thank}}</h2>	
	<p>{{$label_soon}}</p>

	@if($message)
		<p>
			<strong>{{$label_message}}</strong><br>
			{!!nl2br($message)!!}
		</p>
	@endif


	<p>{{$label_greets}}<br>{{$label_company}}</p>
	<p>&nbsp;</p>
	<p><img src="{{get_site_url()}}/{{$logo}}" alt="{{get_bloginfo()}}" width="82" height="123"></p>

	<p>
		<span>{{__($client["name"])}}</span><br>
		<span>{{__($client["street"])}}</span><br>
		<span>{{__($client["zip"])}}</span> <span>{{__($client["city"])}}</span><br>
		<span>{{__($client["country"])}}</span><br>
		<span>{{__($client["tel"])}}</span><br>
		<a href="mailto:{{__($client['email'])}}"><span>{{__($client["email"])}}</span></a>
	</p>

</body>
</html>