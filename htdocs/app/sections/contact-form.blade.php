<?php
    $form_thank    = __('Thank you for your message.');

    $formHelper = Project\Helpers\FormHelper::getInstance();
    $_ajaxHelper   = A365\Wordpress\Helpers\Ajax::getInstance();

?>

<div class="section">
    <div class="section__content">
        <form class="form js-ajax-form js-validate" action="{!!$_ajaxHelper->getUrl('contact-form')!!}" method="POST" data-name='Kontaktformular' data-parsley>
            <div class="form__form">
                <div class="form__fields">
                    
                    <?php
                        echo $formHelper->getFormField("name", __('Name'), ["size" => "full"]);
                        echo $formHelper->getFormField("email", __('E-Mailadresse'), ["type" => "email", "size" => "full"]);
                        echo $formHelper->getFormField("message", __('Nachricht'), ["type" => "textarea", "size" => "full"]);
                        echo $formHelper->getHoneyField();
                    ?>

                    <div class="clearfix"></div>
                </div>

                <div class="form__fields contact-form__submit">
                    
                    <div class="form__fields__row">
                        <div class="form__fields__row__input">
                            <button class="button" type="submit"><?php echo __('Send Form'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form__success">
                <h3 class="h3">{{$form_thank}}</h3>
            </div>
        </form>
    </div>
</div>