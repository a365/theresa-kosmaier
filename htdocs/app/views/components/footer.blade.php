<div class="section section--margin site-footer text">
	<div class="section__content">
		<div class="site-footer__item">
			Theresa Kosmaier | Hannakstraße 9, 5023 Salzburg | Tel.: <a href="tel:+4369910419922">+43 699 10 41 99 22</a> | E-Mail: <a href="mailto:theresa@kosmaier.at">theresa@kosmaier.at</a>
		</div>
	</div>
</div>
