<?php
/**
 * @var \A365\Wordpress\Models\Page $page
 */
?>


<header class="section site-header section--margin">
    <div class="section__content site-header__inner">
        
        {{-- <div class="site-header__item site-header__item--left">
            <div class="site-header__logo">
                @include('components.header.logo-box')
            </div>
        </div>

        <div class="site-header__item site-header__item--right site-navigation">
            <div class="site-navigation__desktop">
                @include('components.header.navigation')
            </div>
            <div class="site-navigation__mobile">
                @include('components.header.menu-burger')
            </div>
        </div> --}}

        <div class="site-header__item">
            <h1 class="h2">Theresa Kosmaier</h1>
        </div>

        
        {{--
        @include('components.header.language-switch')

        @include('components.header.site-search')
        
        
        --}}

    </div>



    {{-- Subnavigation --}}
    {{--
    @include('components.header.subnavigation')
     --}}

</header>

