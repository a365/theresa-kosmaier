<?php

     $headline = $block->getHeadline();
    
?>

<div class="section section--fullwidth">
    <div class="section__content">

        {{$headline}}

        <div class="google-maps">
            <div class='google-maps__map js-google-maps'
                data-disable-default-ui="true"
                data-scrollwheel='false'
                data-zoomcontrol="true"
                data-mapsmarkers="{{ A365\Wordpress\Helpers\AjaxHelper::getInstance()->getUrl('locations') }}"
                >
            </div>
            
        </div>
    </div>
</div>