function GoogleMap($element) {
    this.$element = $element;
    this.init();

    return {
        refresh: this.refresh.bind(this),
        loaded: this.loaded.bind(this),
    };
}

GoogleMap.prototype = new Module();
GoogleMap.prototype.constructor = GoogleMap;

GoogleMap.prototype.vars = {
    map: undefined,
    data: undefined,
    markers: [],
    markers_data: [],
    locations_select: undefined,
    max_zoom: 11,
    loaded: undefined
};

GoogleMap.prototype.classes = {};

GoogleMap.prototype.selectors = {

};

GoogleMap.prototype.elements = {
    icons: undefined,
    locations: undefined,
    close: undefined,
    locations_select: undefined,
    locations_areas: undefined,
};

GoogleMap.prototype.init = function() {

    var self = this;

    this.vars.loaded = jQuery.Deferred();
    this.vars.data = this.$element.data();

    var defaultIcon = {
        url: "/assets/images/layout/maps-marker-padding.png",
        size: new google.maps.Size(32, 32),
        scaledSize: new google.maps.Size(16, 16),
        anchor: new google.maps.Point(8, 8),
    };

    var activeIcon = {
        url: "/assets/images/layout/maps-marker.png",
        size: new google.maps.Size(20, 20),
        //scaledSize: new google.maps.Size(20, 20),
        anchor: new google.maps.Point(10, 10),
    };

    this.elements.icon = { default: defaultIcon, active: activeIcon };



    //self.activateLocation(location_id);
    //self.centerMapToLocation(location_id);

    

    this.initMap();


};

GoogleMap.prototype.loaded = function() {
    return this.vars.loaded.promise();
};

GoogleMap.prototype.getPositionFromLocation = function(location_id) {
    var self = this;
    for (var marker_k in self.vars.markers) {
        if (self.vars.markers[marker_k].marker_id == location_id) {
            return self.vars.markers[marker_k].position;
        }
    }
    return false;
};

GoogleMap.prototype.initMap = function() {

    var self = this;
    var data = this.vars.data;
    var options = this.getMapOptions();

    options.zoom = 5;

    options.styles = [{ "featureType": "all", "elementType": "all", "stylers": [{ "visibility": "simplified" }, { "saturation": -100 }, { "lightness": 73 }, { "gamma": 0.75 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "weight": 1.98 }, { "lightness": 10 }] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "saturation": -51 }, { "lightness": 17 }] }, { "featureType": "road.arterial", "elementType": "labels", "stylers": [{ "visibility": "on" }, { "color": "#b5a7b5" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.arterial", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway", "elementType": "labels.text", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }] }, { "featureType": "road.highway", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.locality", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.locality", "elementType": "labels.text", "stylers": [{ "color": "#000000" }] }, { "featureType": "administrative.locality", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "labels", "stylers": [{ "visibility": "off" }, { "lightness": 54 }] }, { "featureType": "poi", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "weight": 0.69 }, { "lightness": 14 }] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "visibility": "on" }, { "lightness": 37 }] }, { "featureType": "road.local", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }, { "weight": 6.26 }] }, { "featureType": "poi.attraction", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.country", "elementType": "all", "stylers": [{ "visibility": "off" }, { "color": "#ffffff" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#f3e564" }] }, { "featureType": "administrative.country", "elementType": "geometry.stroke", "stylers": [{ "visibility": "on" }, { "color": "#8a8a8a" }, { "weight": 0.86 }] }, { "featureType": "administrative.province", "elementType": "geometry.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "weight": 0.79 }] }, { "featureType": "administrative.locality", "elementType": "labels.text", "stylers": [{ "visibility": "off" }, { "color": "#c7c7c7" }] }, { "featureType": "poi", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "labels.text.fill", "stylers": [{ "color": "#8a8a8a" }] }, { "featureType": "water", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.province", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }];

    this.vars.map = new google.maps.Map(this.$element[0], options);

    this.updateMarkers().then(function() {
        self.vars.loaded.resolve();
    });

};


GoogleMap.prototype.getMapOptions = function() {

    var options = {};
    var data = this.vars.data;
    var value;
    var allowedOptions = ['disableDefaultUI'];

    for (var key in data) {
        value = data[key];

        key = key.replace('Ui', 'UI');
        key = key.replace('control', 'Control');
        key = key.replace('view', 'View');
        key = key.replace('type', 'Type');

        options[key] = value;
    }

    return options;

};

GoogleMap.prototype.activateLocation = function(location_id) {

    var $special_location = this.elements.locations_areas.filter(function() {
        var $this = jQuery(this);
        return $this.data('locationid') == location_id;
    }).eq(0);


    if ($special_location.length) {
        this.elements.locations_areas
            .removeClass('active');

        $special_location
            .addClass('active');
    } else {

        this.elements.locations_areas
            .removeClass('active')
            .filter('.js-google-maps-area-default')
            .addClass('active');

        this.elements.locations
            .removeClass("active")
            .filter(function() {
                return jQuery(this).data("locationid") == location_id;
            })
            .eq(0)
            .addClass("active");
    }


    this.vars.locations_select.setValue(location_id, true);
    this.activateMarker(location_id);
};

GoogleMap.prototype.updateMarkers = function() {

    var defer = jQuery.Deferred();
    var self = this;

    this.fetchMarkers().then(function(markers) {
        self.refreshMarkers(markers);
        
        //self.activateLocation(location_id);
        //self.centerMapToLocation(location_id);
        
    });
    

    return defer;
};




GoogleMap.prototype.fetchMarkers = function() {

    return jQuery.ajax({
        url: this.vars.data.mapsmarkers
    });

};

GoogleMap.prototype.refreshMarkers = function(markers) {

    this.vars.markers_data = markers;

    if (this.vars.markers_data) {
        this.setMarkers();

        if (this.vars.markers.length > 1) {
            this.centerMapToMarkers();
        } else if (this.vars.markers.length > 0) {
            this.centerMapToMarker();
        } else {
            this.centerMapToNull();
        }


    }
};


GoogleMap.prototype.setMarkers = function() {
    var self = this;
    var map = this.vars.map;
    var markers = this.vars.markers_data;
    var markerData;

    for (var key in markers) {
        if (markers.hasOwnProperty(key)) {

            markerData = markers[key].location;

            var options = {
                position: new google.maps.LatLng(markerData.lat, markerData.lng),
                map: map,
                icon: self.elements.icon.default,
                marker_id: markers[key].id.toString(),
                visible: true
            };

            var marker = new google.maps.Marker(options);

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    
                    self.activateMarker(marker.marker_id);
                    

                };
            })(marker, key));

            this.vars.markers.push(marker);
        }
    }


};

GoogleMap.prototype.activateMarker = function(marker_id) {

    for (var j = 0; j < this.vars.markers.length; j++) {
        if (marker_id == this.vars.markers[j].marker_id) {
            this.vars.markers[j].setIcon(this.elements.icon.active);
        } else {
            this.vars.markers[j].setIcon(this.elements.icon.default);
        }
    }
};

GoogleMap.prototype.showMarker = function(marker_id, show_it) {

    for (var j = 0; j < this.vars.markers.length; j++) {
        if (marker_id == this.vars.markers[j].marker_id) {
            this.vars.markers[j].setVisible(show_it);
            this.vars.markers[j].visible = show_it;
            return;
        }
    }
};


GoogleMap.prototype.centerMapToLocation = function(location_id) {

    var position = this.getPositionFromLocation(location_id);
    if (position) {
        this.vars.map.setCenter(position);
    }

};

GoogleMap.prototype.centerMapToMarkers = function() {
    var map = this.vars.map;
    var bounds = new google.maps.LatLngBounds();
    var markers = this.vars.markers;
    var bounds_exist = false;

    for (var i = 0, len = markers.length; i < len; i++) {
        if (!markers[i].visible) continue;
        bounds_exist = true;
        bounds.extend(markers[i].position);
    }
    if (bounds_exist) {
        map.fitBounds(bounds);
        if (map.getZoom() > this.vars.max_zoom) {
            map.setZoom(this.vars.max_zoom);
        }
    }

};


GoogleMap.prototype.centerMapToMarker = function() {
    var map = this.vars.map;
    var markers = this.vars.markers;
    map.setCenter(markers[0].position);

    if (map.getZoom() > this.vars.max_zoom) {
        map.setZoom(this.vars.max_zoom);
    }
};

GoogleMap.prototype.centerMapToNull = function() {
    var map = this.vars.map;

    map.setCenter({lat: 0, lng: 0});

    if (map.getZoom() > this.vars.max_zoom) {
        map.setZoom(this.vars.max_zoom);
    }
};




GoogleMap.prototype.refresh = function() {
    google.maps.event.trigger(this.vars.map, "resize");
};
