<?php
namespace Project\Helpers;

class ImageHelper extends \A365\Core\Abstracts\Helper
{

    private static function _getSize($sc) {
        $sizes = '';

        if (!is_array($sc) || !count($sc)) {
            $sc = array(100);
        }

        if (count($sc) == 1) {
            $sizes .= $sc[0] . 'vw';
        } else if (count($sc) == 2) {
            $sizes .= '(min-width: 48em) ' . $sc[1] . 'vw';
            $sizes .= ', ' . $sc[0] . 'vw';
        } else if (count($sc) == 3) {
            $sizes .= '(min-width: 30em) ' . $sc[1] . 'vw';
            $sizes .= ' ,(min-width: 48em) ' . $sc[2] . 'vw';
            $sizes .= ' , ' . $sc[0] . 'vw';
        } else if (count($sc) == 4) {
            $sizes .= '(min-width: 30em) ' . $sc[1] . 'vw';
            $sizes .= ' ,(min-width: 48em) ' . $sc[2] . 'vw';
            $sizes .= ' ,(min-width: 62em) ' . $sc[3] . 'vw';
            $sizes .= ' , ' . $sc[0] . 'vw';
        } else if (count($sc) == 5) {
            $sizes .= '(min-width: 30em) ' . $sc[1] . 'vw';
            $sizes .= ' ,(min-width: 48em) ' . $sc[2] . 'vw';
            $sizes .= ' ,(min-width: 62em) ' . $sc[3] . 'vw';
            $sizes .= ' ,(min-width: 75em) ' . $sc[4] . 'vw';
            $sizes .= ' , ' . $sc[0] . 'vw';
        }

        return $sizes;
    }


    public static function getPaddingBottom($image_id) {
        $image_meta = wp_get_attachment_metadata($image_id);
        return round($image_meta['height'] / $image_meta['width'] * 100, 4);
    }

    public static function printImage($image_id, $config) {
        $ret = '';
        $responsive = (array_key_exists('sizes', $config));
        $lazyload = (array_key_exists('lazy', $config)) && $config['lazy'];
        $noloader = (array_key_exists('no-loader', $config)) && $config['no-loader'];
        $image_meta = wp_get_attachment_metadata($image_id);

        $imgclass = '';
        if (array_key_exists('class-img', $config)) {
            $imgclass = $config['class-img'];
        }
        $class = '';
        if (array_key_exists('class', $config)) {
            $class = $config['class'];
        }

        if (!$noloader) {
            $ret .= '<div class="image-loader ' . $class . '" style="padding-bottom: ' . self::getPaddingBottom($image_id) . '%">';
        }
        $ret .= '<img alt="' . self::getAltById($image_id) . '" ';
        

        if ($responsive) {
            $sizes = self::_getSize($config['sizes']);
            $image_src = wp_get_attachment_image_src($image_id, 'medium');
            $srcset = wp_get_attachment_image_srcset($image_id);
        } else {
            $image_src = wp_get_attachment_image_src($image_id, 'full');
        }

        if ($lazyload) {
            $ret .= 'data-';
        }
        $ret .= 'src="' . $image_src[0] . '" ';

        if ($responsive) {
            if ($lazyload) {
                $ret .= 'data-';
            }
            $ret .= 'srcset="' . $srcset . '" ';
            $ret .= 'sizes="' . $sizes . '" ';
        }

        $ret .= 'width="' . $image_meta['width'] . '" height="' . $image_meta['height'] . '" class="responsive ' . $imgclass . '" />';
        
        if (!$noloader) {
            $ret .= '</div>';
        }
        
        echo $ret;
    }

    public static function getCaptionById($image_id) {
        $image_meta = get_post($image_id);
        return $image_meta->post_excerpt;
    }

    public static function getDescriptionById($image_id) {
        $image_meta = get_post($image_id);
        return $image_meta->post_content;
    }

    public static function getAltById($image_id) {
        return get_post_meta($image_id, '_wp_attachment_image_alt', true);
    }


    public static function getImageUrlById($image_id, $config = null) {
        $image_src = wp_get_attachment_image_src($image_id, 'full');
        return $image_src[0];
    }

}

