<aside class="sidebar js-sidebar">
	<div class="sidebar__content header-padding">
		@include('components.sidebar.navigation')
	</div>
</aside>
