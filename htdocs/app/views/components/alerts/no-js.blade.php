<?php
$label_headline = __('JavaScript inactive');
$label_text = __('Please activate JavaScript.');
$label_btn = __('I understand');
?>

@if (!isset($_COOKIE['js_alert']))
<noscript>
	<div class="website-alert website-alert--no_script">
		<div class="table">
			<div class="table__td">
				<div class="logo">
					<h2 class="h2">Theresa Kosmaier</h2>
				</div>
				<div class="h3">{{$label_headline}}</div>
				<p>{{$label_text}}</p>
				<br><br>
				<a href="/js-hide.php?url={{$_SERVER["REQUEST_URI"]}}" class="website-alert__btn">{{$label_btn}}</a>
			</div>
		</div>
	</div>
</noscript>
@endif