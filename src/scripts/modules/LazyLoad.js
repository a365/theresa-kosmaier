function LazyLoad($element) {

    return {
        init: this.init.bind(this),
        parse: this.parse.bind(this)
    };
}

LazyLoad.prototype = new Module();
LazyLoad.prototype.constructor = LazyLoad;


LazyLoad.prototype.init = function() {
	$.extend($.lazyLoadXT, {
      edgeY:  500,
      autoInit: false
    });
};


LazyLoad.prototype.parse = function(context) {

    var elements = context.find('[data-bg],[data-src]');

    if (!elements.length){
        return;
    }

    elements.lazyLoadXT({
    });
};

LazyLoad.prototype.withLazyLoad = function() {

};