<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<h2>Neue Kontaktanfrage</h2>
	<p>
		<strong>Anrede: </strong> @if($salutation) Frau @else Herr @endif<br>
		<strong>Name: </strong> {{$firstname}} {{$lastname}}<br>
		@if($company)
			<strong>Firma: </strong> {{$company}}<br>
		@endif
		<strong>E-Mail: </strong> {{$email}}<br>
		<strong>Straße: </strong> {{$street}}<br>
		<strong>Ort: </strong> {{$zip}} {{$city}}<br>
		<strong>Land: </strong> {{$country}}<br>
		@if($lang)
			<strong>Sprache: </strong> {{$lang}}<br>
		@endif

		<strong>Interessiert: </strong> {{$interested}}<br>

		@if($orderbrochures)
			<strong>Muster und Broschüren: </strong> {{$orderbrochureslist}}<br>
		@endif

		@if($newsletter)
			<strong>Newsletter: </strong> Ja!<br>
		@endif
	</p>

	<h3>Nachricht</h3>
	<p>{!!nl2br($message)!!}</p>

</body>
</html>