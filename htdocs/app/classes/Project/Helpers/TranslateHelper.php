<?php

namespace Project\Helpers;

class TranslateHelper extends \A365\Core\Abstracts\Helper
{

    private $_sync_types = array("checkbox", "image", "gallery", "true_false", "radio", "color_picker", "date_picker", "date_time_picker", "time_picker", "oembed", "select", "google_map", "taxonomy");
    private $_sync_types_post = array("post_object", "page_link", "relationship");
    private $_sync_types_flexible = array("flexible_content");
    private $_sync_types_repeater = array("repeater");
    private $_sync_types_text = array('text', 'textarea', 'wysiwyg');
    private $_saved_ids = array();
    private $_saved_texts = array();

    public function __construct() {
        $this->_printStyles();
    }

    private function _printStyles() {
        ?>
            <style>
                body {
                    font-family: 'Arial';
                }
                .section-headline {
                    font-size: 3em;
                    font-weight: bold;
                }
                .post-headline {
                    font-size: 2em;
                    font-weight: bold;
                }
            </style>
        <?php
    }

    private function _syncModel($id) {

        if (in_array($id, $this->_saved_ids)) {
            return;
        } else {
            $this->_saved_ids[] = $id;
        }

        $post_fields = get_field_objects($id);

        if (!isset($post_fields) || !is_array($post_fields)) return;

        foreach($post_fields as $key => $post_field) {


            if ($key == "page_content" || $key == "content") {

                if (!$post_field) {
                    return;
                }

                if (!is_array($post_field["value"])) {
                    return;
                }

                if (!array_key_exists('layouts', $post_field)) {
                    return;
                }


                foreach($post_field["value"] as $fieldvalue) {

                    $layout_subfields = false;
                    foreach($post_field['layouts'] as $layout) {
                        if ($layout['name'] == $fieldvalue['acf_fc_layout']) {
                            $layout_subfields = $layout['sub_fields'];
                            break;
                        }
                    }

                    if (!$layout_subfields) {
                        continue;
                    }

                    $this->_syncFields($fieldvalue, $id, $layout_subfields);
                }
            } else {
                $type = $post_field['type'];
                $value = $post_field['value'];
                $name = $post_field['name'];
                $label = $post_field['label'];
                if (in_array($type, $this->_sync_types_text)) {
                    if (!is_numeric($value) && strlen($value)) {
                       $this->_syncTexts($id, $name, $label, $value);
                    }
                }
            }
        }
    }



    private function _syncFields($fieldvalue, $id, $layout_subfields) {

        foreach($layout_subfields as $field) {
            if (!strlen($field['name'])) continue;
            if (!array_key_exists($field['name'], $fieldvalue)) continue;
            $value = $fieldvalue[$field['name']];
            $type = $field['type'];
            $label = $field['label'];
            if (in_array($type, $this->_sync_types_text)) {
                if (!is_numeric($value) && strlen($value)) {
                   $this->_syncTexts($id, $field['name'], $label, $value);
                }
                
            } else if (in_array($type, $this->_sync_types_repeater)) {
                if ($value) {
                    foreach($value as $v) {
                        $this->_syncFields($v, $id, $field['sub_fields']);
                    }
                }
                
                
            } else if (in_array($type, $this->_sync_types_flexible)) {
                if ($value) {
                    foreach($value as $v) {
                        $acf_layout = $v["acf_fc_layout"];

                        foreach($field['layouts'] as $layout) {
                            if ($layout["name"] == $acf_layout) {
                                $this->_syncFields($v, $id, $layout['sub_fields']);
                                break;
                            }
                        }
                    }
                }

            }
        }

    }

    private function _formatText($text) {
        return $text;
    }

    private function _syncTexts($id, $name, $label, $value) {
        if ($name == "acf_sync_id") return;

        $duplicate = "Übersetzung";
        $color = 'background-color: orange; border: 1px solid #000';
        $color2 = '';

        if (in_array($value, $this->_saved_texts)) {
            $duplicate = "Duplikat";
            $color = '';
            $color2 = 'color: #bbb';
        } else {
            $this->_saved_texts[] = $value;
        }

        echo "<tr style='" . $color2 . "'>";
        echo "<td>" . $id . "</td>";
        echo "<td>" . $name . "</td>";
        echo "<td>" . $label . "</td>";
        echo "<td>" . $this->_formatText($value) . "</td>";
        echo "<td style='" . $color . "'></td>";
        echo "<td style='" . $color . "'></td>";
        echo "</tr>";
    }

    private function _printSectionHeadline($headline) {
        echo "<tr><td></td><td></td><td></td><td><br><br><br><div class='section-headline'>" . $headline . "</div></td></tr>";
    }

    private function _printPostHeadline($post, $public = true) {
        echo "<tr><td></td><td></td><td></td><td><br><br><div class='post-headline'>" . $post->getPostTitle() . "</div>";
        if ($public) {
            echo "<div><a target='_blank' href='" . get_permalink($post->getId()) . "'>" . get_permalink($post->getId()) . "</a></div>";
        }
        echo "<br></td></tr>";
    }

    public function printPageList($pages) {
        echo "<table>";
        $this->_printSectionHeadline('Seiten');
        foreach($pages as $page) {
            $this->_printPostHeadline($page);
            foreach($page->getSectionIds() as $section) {
                $this->_syncModel($section);
            }
        }
        echo "</table>";
    }

    public function printModelList($list, $headline, $public = true) {
        echo "<table>";
        $this->_printSectionHeadline($headline);
        foreach($list as $n) {
            $this->_printPostHeadline($n, $public);
            $this->_syncModel($n->getId());
        }
        echo "</table>";
    }


    

}


