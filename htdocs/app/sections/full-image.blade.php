<?php
$i = $block->getImage();
?>
<div class="section">
    <div class="section__content">
        @php
            \Project\Helpers\ImageHelper::printImage($i, ['sizes' => [100], 'lazy' => true]);
        @endphp
    </div>
</div>