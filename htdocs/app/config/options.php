<?php
$config = [
    "acf" => [
        'options' => [
            [
                'page_title' => 'Stammdaten',
                'menu_slug'  => 'company-infos',
                'icon_url'   => 'dashicons-performance',
            ],
            [
                'page_title'  => 'Stammdaten',
                'parent_slug' => 'company-infos'
            ],
        ]
    ]
];