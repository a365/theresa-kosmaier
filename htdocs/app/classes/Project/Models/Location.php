<?php 
namespace Project\Models;

class Location extends \A365\Wordpress\Models\Post {
    protected static $order = 'ASC';
    protected static $orderby = 'menu_order';

    public static function allPublished($limit = -1, $options = NULL)
    {
        $locations = parent::allPublished($limit, $options);

        return $locations;
    }

    public function toArray() {

        $data = [
            'id' => $this->getId(),
            'post_title' => $this->getPostTitle(),
        ];
        $data = array_merge($data, $this->getMetaData());

        foreach ($data as $key => $value) {

            if(strpos($key, '_') === 0) {
                unset($data[$key]);
            }
        }

        return $data;
    }

}