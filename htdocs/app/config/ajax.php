<?php
$config = [
    "ajax" => [
        "actions" => [
            'contact-form' => '\\Project\\Ajax\\Actions\\ContactForm',
            'locations' => '\\Project\\Ajax\\Actions\\Locations',
        ],
    ],
];
