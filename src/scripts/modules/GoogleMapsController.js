function GoogleMapsController() {
    return {
        init: this.init.bind(this),
        parse: this.parse.bind(this),
        refresh: this.refresh.bind(this),
        loaded: this.loaded.bind(this),
    };
}

GoogleMapsController.prototype = new Module();
GoogleMapsController.prototype.constructor = GoogleMapsController;

GoogleMapsController.prototype.classes = {};

GoogleMapsController.prototype.selectors = {
    maps: 'js-google-maps'
};

GoogleMapsController.prototype.variables = {
    maps: undefined,
    loaded: undefined,
    googleMapsKey: 'AIzaSyBbmPybvMqrhd6jDQMnWGnZkH8mlkuc680'
};

GoogleMapsController.prototype.elements = {
    maps: undefined
};

GoogleMapsController.prototype.init = function() {

    this.elements.maps = jQuery(this.getSelector('maps'));

    if (!this.elements.maps.length) {
        return false;
    }

    this.variables.loaded = jQuery.Deferred();

    this.loadLibrary();

};

GoogleMapsController.prototype.parse = function($context) {

};

GoogleMapsController.prototype.loadLibrary = function() {

    window.libraryCallback = this.libraryCallback.bind(this);

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&language=de&callback=libraryCallback&key=' + this.variables.googleMapsKey;

    document.body.appendChild(script);
};

GoogleMapsController.prototype.libraryCallback = function() {
    this.initMaps();
};

GoogleMapsController.prototype.initMaps = function() {
    var self = this;

    this.variables.maps = [];
    var promise_array = [];

    for (var i = this.elements.maps.length - 1; i >= 0; i--) {
        var map = new GoogleMap(this.elements.maps.eq(i));
        promise_array.push(map.loaded());
        this.variables.maps.push(map);
    }

    jQuery.when.apply($, promise_array).done(function() {
        self.variables.loaded.resolve();
    });

};

GoogleMapsController.prototype.loaded = function() {
    return this.variables.loaded.promise();
};

GoogleMapsController.prototype.refresh = function() {

    if (!this.elements.maps.length) {
        return false;
    }
    var self = this;
    return jQuery.when(this.loaded()).then(function() {

        for (var map_key in self.variables.maps) {
            self.variables.maps[map_key].refresh();
        }

    });

};
