<?php
namespace Project;

use A365\Wordpress\Application as BaseApplication;

class Application extends BaseApplication
{

	protected function always() 
	{
		parent::always();

		
		add_action( 'init', [$this, 'rewriteRules'] );
		add_action( 'template_redirect', [$this, 'prefix_url_rewrite_templates'] );

		add_action('after_setup_theme', [$this, 'loadTextdomain']);
		add_filter('wp_get_attachment_url', [$this, 'ssl_post_thumbnail_urls'], 10, 2);
		add_action( 'wp_footer', [$this, 'my_deregister_scripts'] );

		add_action('acf/init', [$this, 'myAcfInit']);
	}
	


	protected function frontend()
	{
		parent::frontend();

		add_filter('posts_join', [$this, 'cf_search_join'] );
		add_filter( 'posts_where', [$this, 'cf_search_where'] );
		add_filter( 'posts_distinct', [$this, 'cf_search_distinct'] );
		add_action('pre_get_posts', [$this, 'search_filter'] );

		add_filter( 'max_srcset_image_width', [$this, 'filter_max_srcset_image_width'], 10, 2 ); 
		
	}

	protected function backend()
	{
		parent::backend();

		
		add_filter('upload_mimes', [$this, 'cc_mime_types']);
		add_filter('tiny_mce_before_init', [$this, 'my_mce_before_init_insert_formats']);

	}

	public function my_mce_before_init_insert_formats( $init_array ) {  
	    $default_style_formats = array(
	        array(
	            'title'   => 'Überschriften',
	            'items' => array(
	                array(
	                    'title'   => 'Überschrift 1',
	                    'format'  => 'h1',
	                ),
	                array(
	                    'title'   => 'Überschrift 2',
	                    'format'  => 'h2',
	                ),
	                array(
	                    'title'   => 'Überschrift 3',
	                    'format'  => 'h3',
	                ),
	                array(
	                    'title'   => 'Überschrift 4',
	                    'format'  => 'h4',
	                ),
	                array(
	                    'title'   => 'Normaler Absatz',
	                    'format'  => 'p',
	                ),
	            ),
	        ),
	    );

	    $custom_style_formats = array(
	        /*array(
	            'title'   => 'Buttons',
	            'items' => array(
	                array(
	                    'title'   => 'Button',
	                    'block'   => 'div',
	                    'classes' => 'btn',
	                ),
	                
	            ),
	        ),*/
	    );
	 
	    $new_style_formats = array_merge( $default_style_formats, $custom_style_formats );
	    $init_array['style_formats'] = json_encode( $new_style_formats );
	    return $init_array;
	  
	} 

	public function filter_max_srcset_image_width( $int, $size_array ) { 
	    return 2400; 
	}

	public function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  $mimes['dwg'] = 'application/dwg';
	  return $mimes;
	}


	public function rewriteRules() {

		$this->add_rewrite_rule('^all-contents/?', 'index.php?mypagename=all-contents', 'top');
	}

	public function prefix_url_rewrite_templates() {
		
	    if ( get_query_var( 'mypagename' ) == "all-contents") {

	        add_filter( 'template_include', function() {
	            return get_template_directory() . '/all-contents.php';
	        });
	    }
	}

	public function my_deregister_scripts(){
	  wp_deregister_script( 'wp-embed' );
	}

	public function myAcfInit()
	{
		acf_update_setting('google_api_key', $this->getConfig('google.api.key'));
	}

	public function loadTextdomain()
	{
		load_theme_textdomain('default', ABSPATH . 'app/langs');
	}

	//Fix SSL on Post Thumbnail URLs
   public function ssl_post_thumbnail_urls($url, $post_id) {
		//Skip file attachments
		if( !wp_attachment_is_image($post_id) )
		     return $url;

		//Correct protocol for https connections
		list($protocol, $uri) = explode('://', $url, 2);
		if( is_ssl() ) {
	        if( 'http' == $protocol )
	                $protocol = 'https';
		} else {
	        if( 'https' == $protocol )
	                $protocol = 'http';
		}
		return $protocol.'://'.$uri;
    }

	

	private function add_rewrite_rule($regex, $redirect, $after)
	{
		if (function_exists("pll_languages_list")) {

			$lang_list = pll_languages_list();
			$new_regex = $regex;
			$start_removed = false;
			if (strpos($regex, '^') == 0) {
				$regex = substr($regex, 1);
				$start_removed = true;
			}

			foreach($lang_list as $lang) {
				$new_regex = $lang . "/" . $regex;
				if ($start_removed) {
					$new_regex = "^" . $new_regex;
				}
				add_rewrite_rule($new_regex, $redirect, $after);
			}

			
		} else {
			add_rewrite_rule($regex, $redirect, $after);
		}
	}


	/**
	 * Extend WordPress search to include custom fields
	 *
	 * https://adambalee.com
	 */

	/**
	 * Join posts and postmeta tables
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
	 */
	public function cf_search_join( $join ) {
	    global $wpdb;

	    if ( is_search() ) {

	        $join = str_replace("INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )", "", $join);
	        $join .=' LEFT JOIN ' . $wpdb->postmeta . ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	    }

	    return $join;
	}


	/**
	 * Modify the search query with posts_where
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
	 */
	public function cf_search_where( $where ) {
	    global $pagenow, $wpdb;

	    if ( is_search() ) {
	        $where = preg_replace(
	            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
	            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
	    }

	    return $where;
	}


	/**
	 * Prevent duplicates
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
	 */
	public function cf_search_distinct( $where ) {
	    global $wpdb;

	    if ( is_search() ) {
	        return "DISTINCT";
	    }

	    return $where;
	}


	public function search_filter($query) {
	  if ( !is_admin() && $query->is_main_query() ) {
	    if ($query->is_search) {
	        $post_types = array();
	        $customPostTypes = \Project\Application::getInstance()->getConfig('customPostTypes');
	        foreach($customPostTypes as $k => $v) {
	            $post_types[] = $k;
	        }
	        $query->set('post_type', $post_types );
	    }
	  }
	}

}


