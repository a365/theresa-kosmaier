<?php

/**
 * @var \Project\Sections\ContactLocations $block
 */
$gallery = $block->getGallery();

?>
<div class="section">
    <div class="section__content">
        <div class="gallery">
            <div class="gallery__wrapper @if(count($gallery) > 1)js-owl-carousel @endif">
                @foreach($gallery as $image) 
                    <div class="gallery__item bg-image" style="background-image: url('{{$image['url']}}')"></div>
                @endforeach
            </div>
        </div>
    </div>
</div>