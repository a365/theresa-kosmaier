<?php
namespace Project\Sections;

use A365\Wordpress\Helpers\Acf\FieldHelper;

class Gallery extends \A365\Wordpress\Block\AcfBlock {

	protected $_template = 'gallery';
	protected static $_label = 'Slideshow';

	/**
	 * @inheritdoc
	 */
	public static function getAcfSubfields()
	{
		return [
			array (
				'library' => 'all',
				'min' => 1,
				'max' => '',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'insert' => 'append',
				'label' => 'Galerie',
				'name' => 'gallery',
				'type' => 'gallery',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
			),
		];
	}
}