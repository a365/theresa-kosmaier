<?php
/**
 * @var \A365\Wordpress\Helpers\MenuHelper $menuHelper
 * @var array $items Menu Items
 */

$menuHelper = \Project\Helpers\MenuHelper::getInstance();
$nav_items      = $menuHelper->getMenuItems('footer');

?>
<nav class="nav nav--footer">
    <ul class="nav__list nav__list--level-1">
        @each("components.navigation.item", $nav_items['branch'], 'nav_item')
    </ul>
</nav>