<?php
$config = [
	'type' => 'incompatible_browser'
];
$label_old_browser = __('You are using an incompatible browser.');
$label_modern = __('Modern Browsers are');
?>


<div class="website-alert website-alert--browser js-alert" data-config='{!!json_encode($config)!!}'>
	<div class="table">
		<div class="table__td">
			<div class="logo">
				<h2 class="h2">Theresa Kosmaier</h2>
			</div>
			<div class="h3">{{$label_old_browser}}</div>
			<p>{{$label_modern}}:</p>
			<table>
				<tr>
					<td>
						<a href="http://www.firefox.com/" target="_blank" alt="Firefox Browser Logo">
							<img class="website-alert__image" src="assets/images/browser/firefox.png">
							<p>Mozilla Firefox</p>
						</a>
					</td>
					<td>
						<a href="http://www.google.com/chrome" target="_blank" alt="Chrome Browser Logo">
							<img class="website-alert__image" src="assets/images/browser/chrome.png">
							<p>Google Chrome</p>
						</a>
					</td>
				</tr>
			</table>

		</div>
	</div>
</div>