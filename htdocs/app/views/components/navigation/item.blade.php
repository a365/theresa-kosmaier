<li class="nav__item nav__item--level-{{$nav_item->level}} {{$nav_item->getClass()}}" data-id='{{$nav_item->post_id}}'>
    @if(!$nav_item->getLink())
        <div class="nav__item__headline nav__item__headline--no-link">{{$nav_item->title}}</div>
    @else
       <a class="nav__item__headline" href="{{$nav_item->getLink()}}">{{$nav_item->title}}</a>
    @endif

    @if (isset($nav_item->submenu))
        <ul class="nav__list nav__list--level-{{$nav_item->level + 1}}">
            @each("components.navigation.item", $nav_item->submenu, 'nav_item')
        </ul>
    @endif
</li>
