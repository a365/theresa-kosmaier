<?php
namespace Project\Helpers;

class OgHelper extends \A365\Core\Abstracts\Helper {

    private $_og_post = false;

    private $_sync_types_flexible = array("flexible_content");
    private $_sync_types_repeater = array("repeater");
    private $_sync_types_text = array('text', 'textarea', 'wysiwyg');
    private $_sync_types_images = array('image', 'image_crop');
    private $_sync_types_gallery = array('gallery');
    private $_size = 'full';
    private $_convert = "src";

    public function setSize($size) {
        $this->_size = $size;
    }

    public function getImage($id = null) {
        $images = $this->getImagesOfPost($id);
        if ($images) {
            return $images[0];
        }
        return false;
    }

    public function getImages($id = null) {
        return $this->getImagesOfPost($id);
    }


    public function getImagesOfPost($id = null) {
        if (!isset($id)) {
            $id = get_the_ID();
        }
        $post_type = get_post_type($id);

        if ($post_type == "page") {
            $images = $this->_getPageImages($id);
        } else {
            $images = $this->_getModelImages($id);
        }

        return array_values(array_unique(array_filter($images)));
    }

     public function getImagesAll() {

        $this->_convert = "id";
        $images_all = array();

        $post_types = \A365\Wordpress\Config::getInstance()->getItem("acf.page_link.default_post_types", array('page'));

        foreach($post_types as $post_type) {

            $my_posts = get_posts(['post_status' => 'publish,private,draft', 'posts_per_page'=> -1, 'post_type' => $post_type]);
            
            
            foreach($my_posts as $my_post) {
                $id = 0;
                if (is_integer($my_post)) {
                    $id = $my_post;
                } else if (is_object($my_post)) {
                    $id = $my_post->ID;
                }

                $images = $this->getImagesOfPost($id);
                $images_all = array_merge($images_all, $images);
            }
        }

        return array_values(array_unique(array_filter($images_all)));
    }


    public function print_it() {
        
        $images = $this->getImagesOfPost();
        $ret = '';
        
        if ($images) {
            foreach($images as $image) {
                $ret .= '<meta property="og:image" content="' . $image . '" />';
            }
        }
        
        echo $ret;

    }

    private function _getPageImages($page_id) {
        $page = \A365\Wordpress\Models\Page::find($page_id);
        $images = array();
        foreach($page->getSectionIds() as $section) {
            $model_images = $this->_getModelImages($section);
            if (is_array($model_images)) {
                $images = array_merge($images, $model_images); 
            }
        }
        return $images;
    }

    private function _getModelImages($id) {

        $post_fields = get_field_objects($id);
        $images = array();

        if (!isset($post_fields) || !is_array($post_fields)) return $images;

        foreach($post_fields as $key => $post_field) {

            if ($key == "page_content" || $key == "content") {


                if (!$post_field) {
                    continue;
                }

                if (!is_array($post_field["value"])) {
                    continue;
                }

                if (!array_key_exists('layouts', $post_field)) {
                    continue;
                }


                foreach($post_field["value"] as $fieldvalue) {

                    $layout_subfields = false;
                    foreach($post_field['layouts'] as $layout) {
                        if ($layout['name'] == $fieldvalue['acf_fc_layout']) {
                            $layout_subfields = $layout['sub_fields'];
                            break;
                        }
                    }

                    if (!$layout_subfields) {
                        continue;
                    }

                    $field_images = $this->_syncFields($fieldvalue, $id, $layout_subfields);
                    $images = array_merge($images, $field_images); 

                    
                }
            } else {
                $type = $post_field['type'];
                $value = $post_field['value'];
                if (in_array($type, $this->_sync_types_images)) {
                    $images[] = $this->_convertImage($value);
                }
                if (in_array($type, $this->_sync_types_gallery)) {
                    if (is_array($value)) {
                        foreach($value as $img) {
                            $images[] = $this->_convertImage($img["id"]);
                        }
                    }
                }
            }
        }
        return $images;
    }

    private function _convertImage($image) {


        if ($this->_convert == "id") {
            return $this->_convertImageToId($image);
        } else {
            return $this->_convertImageToSrc($image);
        }
        

        
    }

    private function _convertImageToId($image) {


        if (is_numeric($image)) {
            return $image;
        }
        if (is_array($image)) {
            return $image["id"];
        }
        if (is_string($image)) {
            return $this->get_attachment_id($image);
        }
        
        return null;
    }

    private function _convertImageToSrc($image) {

        if (is_numeric($image)) {
            $image = wp_get_attachment_image_src($image, $this->_size)[0];
        }
        if (is_string($image)) {
            return $image;
        }

        return null;
    }

    private function get_attachment_id( $url ) {
        $attachment_id = 0;
        $dir = wp_upload_dir();
        if ( false !== strpos( $url, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?
            $file = basename( $url );
            $query_args = array(
                'post_type'   => 'attachment',
                'post_status' => 'inherit',
                'fields'      => 'ids',
                'meta_query'  => array(
                    array(
                        'value'   => $file,
                        'compare' => 'LIKE',
                        'key'     => '_wp_attachment_metadata',
                    ),
                )
            );
            $query = new WP_Query( $query_args );
            if ( $query->have_posts() ) {
                foreach ( $query->posts as $post_id ) {
                    $meta = wp_get_attachment_metadata( $post_id );
                    $original_file       = basename( $meta['file'] );
                    $cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
                    if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
                        $attachment_id = $post_id;
                        break;
                    }
                }
            }
        }
        if ($attachment_id == 0) {
            return $url;
        }
        return $attachment_id;
    }



    private function _syncFields($fieldvalue, $id, $layout_subfields) {

        $images = array();

        foreach($layout_subfields as $field) {
            if (!strlen($field['name'])) continue;
            if (!array_key_exists($field['name'], $fieldvalue)) continue;
            $value = $fieldvalue[$field['name']];
            $type = $field['type'];
            $label = $field['label'];
            if (in_array($type, $this->_sync_types_images)) {
                $images[] = $this->_convertImage($value);
                
            } else if (in_array($type, $this->_sync_types_gallery)) {
                if (is_array($value)) {
                    foreach($value as $img) {
                        $images[] = $this->_convertImage($img["id"]);
                    }
                }
            } else if (in_array($type, $this->_sync_types_repeater)) {
                if ($value) {
                    foreach($value as $v) {
                        
                        $field_images = $this->_syncFields($v, $id, $field['sub_fields']);
                        $images = array_merge($images, $field_images); 
                    }
                }
                
                
            } else if (in_array($type, $this->_sync_types_flexible)) {
                if ($value) {
                    foreach($value as $v) {
                        $acf_layout = $v["acf_fc_layout"];

                        foreach($field['layouts'] as $layout) {
                            if ($layout["name"] == $acf_layout) {
                                $field_images = $this->_syncFields($v, $id, $layout['sub_fields']);
                                $images = array_merge($images, $field_images); 
                                break;
                            }
                        }
                    }
                }

            }
        }

        return $images;

    }

    
}