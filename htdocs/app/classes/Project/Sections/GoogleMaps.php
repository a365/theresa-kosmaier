<?php
namespace Project\Sections;

use A365\Wordpress\Helpers\Acf\FieldHelper;

class GoogleMaps extends \A365\Wordpress\Block\AcfBlock {

	protected $_template = 'google-maps';
	protected static $_label = 'Google Maps';

	/**
	 * @inheritdoc
	 */
	public static function getAcfSubfields()
	{
		return [
			FieldHelper::createText('headline', 'Überschrift')->setRequired()
		];
	}
}