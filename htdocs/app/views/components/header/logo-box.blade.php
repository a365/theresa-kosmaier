<?php
$logo = Project\Application::getInstance()->getConfig('media.logo');
?>
<a href="{{home_url()}}" class="logo-box" title="{{get_bloginfo()}}">
    <img src="{{$logo}}" alt="{{get_bloginfo()}} Logo" />
</a>