@if (isset($location))
	<div class="site-footer__address">
		<address class="address">
			<div class="address__group">
				<div class="address__headline">	
					<span>{{$headline}}</span>
				</div>
			</div>
			<div class="address__group">
				@if ($location->getTitle())
					<span>{{$location->getTitle()}}</span><br>
				@endif
				<span>{{$location->getStreet()}}</span><br>
				<span class="address__zip">{{$location->getZip()}}</span> <span>{{$location->getCity()}}</span><br>
				<span>{{$location->getCountry()}}</span>
			</div>
			<div class="address__group">
				@if ($location->getTel())
					<span>{{$location->getTel()}}</span><br>
				@endif
				@if ($location->getMobile())
					<span>{{$location->getMobile()}}</span><br>
				@endif
				<a class="bold-link" href="mailto:{{$location->getEmail()}}">{{$location->getEmail()}}</a>
			</div>
		</address>
	</div>
@endif