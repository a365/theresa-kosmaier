<?php
namespace Project\Sections;

use A365\Wordpress\Helpers\Acf\FieldHelper;

class FullImage extends \A365\Wordpress\Block\AcfBlock {

	protected $_template = 'full-image';
	protected static $_label = 'Bild (volle Breite)';

	/**
	 * @inheritdoc
	 */
	public static function getAcfSubfields()
	{
		return [
			FieldHelper::createImage('image', 'Bild', ["library" => "all"])->setRequired(),
		];
	}
}