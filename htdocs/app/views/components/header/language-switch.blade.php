<div class="language-switch language-switch--main js-language-switch">
	<div class="language-switch__current js-language-switch__current"></div>
	<ul>
		<?php pll_the_languages(array('show_flags' => '1', 'display_names_as' => 'name')); ?>
	</ul>
</div>